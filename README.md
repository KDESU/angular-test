﻿# Angular Test

## Introduction

### Application Overview

The application is made of two components:
- A .Net Core Web Api
    - With endpoints for Products and Categories. 
    - Products and Categories data is stored in a postgres database. 
    - Database communication is handled using the Entity Framework ORM with Code First.  
- An angular app which calls the Web API and displays the list of products in a table

### Scripts

The database can be recreated and reseeded by running the scripts/recreatedb.cmd script. Use this script if you want to restore the database to the initial state.

## Tasks

Clone the code from this repository and run the product app to view the list of products. You'll need to run the api project to view the products. Please implement the following functionality.

1. The products currently don't display the category. Make a change in the products-list component so that the categories are displayed. **Note:** No changes need to be made on the backend for this task.
2. Implement filtering in table – Make a change in product list so that whever a category is selected, the table should display only selected category items. Reuse existing code if required. **Note:** No changes need to be made on the backend for this task.
3. Implement “Delete” functionality – When the user clicks on delete the product should get deleted from database. **Note:** you don't need to implement auto refreshing the list of products after deleting. You can just refresh the browser to show it working.
4. Create two new components product-container component and product-detail component.
    - Product container component should contain the existing product-list component and the newly created product-detail component
    - The product-detail component can contain any markup as you will implement its functionality in the next task.
    - When the application runs, the product-container component should be displayed instead of the product-list component.
5. The product-detail component should display the product details when the "view details" button is clicked on the product-list component. **Note:** that no changes need to be made on the backend for this task. The product detail component should display the following information
    1.	Product Name
    2.	Product Code
    3.	Description
    4.	Price
    5.	Quantity in stock
6. Write some unit tests for the product-list component. 
