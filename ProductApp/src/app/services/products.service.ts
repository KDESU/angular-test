import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/product';
import { combineLatest, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProductCategory } from '../models/product-category';

@Injectable({ providedIn: 'root' })
export class ProductsService {
  baseUrl = 'http://localhost:50311/';
  productsUrl = `${this.baseUrl}api/products`;
  categoriesUrl = `${this.baseUrl}api/categories`;

  constructor(private http: HttpClient) {}

  products$ = this.http.get<Product[]>(this.productsUrl);

  categories$ = this.http.get<ProductCategory[]>(this.categoriesUrl);

  productsWithCategories$ = combineLatest(
    this.products$,
    this.categories$
  ).pipe(
    map(([products, categories]) =>
      products.map(product => ({
        ...product,
        category: product.category = categories.find(
          category => category.id === product.categoryId
        ).name
      }))
    )
  );

  private selectedCategorySubject = new BehaviorSubject(0);

  selectedCategoryId$ = this.selectedCategorySubject.asObservable();

  filteredProducts$ = combineLatest(
    this.productsWithCategories$,
    this.selectedCategoryId$
  ).pipe(
    map(([products, categoryId]) =>
      products.filter(product =>
        categoryId ? product.categoryId === categoryId : true
      )
    )
  );

  selectCategory(categoryId: number) {
    this.selectedCategorySubject.next(categoryId);
  }
}
