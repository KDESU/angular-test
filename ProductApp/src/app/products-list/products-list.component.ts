import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Product } from '../models/product';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent {
  products$ = this.productsService.products$;

  categories$ = this.productsService.categories$;

  constructor(private productsService: ProductsService) {}
}
