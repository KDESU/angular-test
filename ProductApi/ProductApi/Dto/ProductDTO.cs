﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductApi.DTO
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }

        public double Price { get; set; }
        public int? CategoryId { get; set; }

        public string Category { get; set; }
        public int QuantityInStock { get; set; }
       
    }
}
