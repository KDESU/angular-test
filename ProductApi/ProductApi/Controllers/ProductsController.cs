﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProductApi.DTO;
using ProductApi.Models;
using ProductApi.Queries;
using ProductApi.Repositories;

namespace ProductApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProductContext _context;
        private GenericRepository<Product> _productRepository;
        private readonly IMapper _mapper;

        public ProductsController(GenericRepository<Product> productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper = mapper;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<IEnumerable<ProductDto>> GetProducts()
        {
            var products =await  _productRepository.GetAll();
            return _mapper.Map<IEnumerable<ProductDto>>(products);
        }
      
    }
}