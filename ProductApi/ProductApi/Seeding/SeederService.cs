﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductApi.Seeding
{
    public class SeederService
    {
        private readonly ProductContext _db;
        private IProductCategoryProvider _productCategoryProvider;

        public SeederService(ProductContext db, IProductCategoryProvider productCategoryProvider)
        {
            _db = db;
            _productCategoryProvider = productCategoryProvider;
        }

        public void Seed()
        {
            if (AlreadySeeded()) return;
            SeedProducts();
        }

        private bool AlreadySeeded()
        {
            return _db.Categories.Any() ||
                   _db.Products.Any();

        }

        private void SeedProducts()
        {
            _productCategoryProvider.SetData();

            _db.Categories.AddRange(_productCategoryProvider.Categories);
            _db.Products.AddRange(_productCategoryProvider.Products);
            _db.SaveChanges();
        }

    }
}
