﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductApi.Models;

namespace ProductApi.Seeding
{
    public interface IProductCategoryProvider
    {
        void SetData();
        IEnumerable<Product> Products { get;  }
        IEnumerable<Category> Categories { get;  }
    }
    public class ProductCategoryProvider : IProductCategoryProvider
    {
        public IEnumerable<Product> Products { get; private set; }
        public IEnumerable<Category> Categories { get; private set; }

        public void SetData()
        {
            Categories = GetCategories();
            Products = GetProducts();

        }

        List<Category> GetCategories()
        {
            var list = new List<Category>();
            list.Add(new Category(){ Id = 1 ,Name = "Garden"});
            list.Add(new Category() { Id = 2, Name = "Toolbox" });
            list.Add(new Category() { Id = 3, Name = "Gaming" });
            return list;
        }

        List<Product> GetProducts()
        {
            var list = new List<Product>();
            list.Add(new Product() { Id= 1,ProductName= "Leaf Rake",ProductCode= "GDN-0011",Description= "Leaf rake with 48-inch wooden handle",Price= 19.95,CategoryId= 1,QuantityInStock= 15,});
            list.Add(new Product() { Id = 2, ProductName = "Hammer", ProductCode = "TBX-0048", Description = "Curved claw steel hammer", Price = 9.95, CategoryId = 2, QuantityInStock = 15, });
            list.Add(new Product() { Id = 3, ProductName = "Video Game Controller", ProductCode = "GMG-0042", Description = "Standard two-button video game controller", Price = 19.95, CategoryId = 3, QuantityInStock = 15, });
            list.Add(new Product() { Id = 4, ProductName = "Garden Cart", ProductCode = "GDN-0023", Description = "15 gallon capacity rolling garden cart", Price = 32.95, CategoryId = 1, QuantityInStock = 15, });
            
            return list;
        }
    }
}
