﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using ProductApi.Models;

namespace ProductApi.Repositories
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        private ProductContext _db;
        private DbSet<TEntity> _table;
        private readonly IMapper _mapper;

        public GenericRepository(ProductContext db, IMapper mapper)
        {
            _db = db;
            _table = db.Set<TEntity>();
            _mapper = mapper;
        }

        public async Task Create(TEntity value)
        {
            _db.Add(value);
            await _db.SaveChangesAsync();
        }


      
        public async Task CreateRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
                _db.Attach(entity).State = EntityState.Added;

            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAll(Func<DbSet<TEntity>, IQueryable<TEntity>> query = null)
        {
            return await GetEntities(query).ToListAsync();
        }

        public async Task<IEnumerable<TResult>> GetAll<TResult>(Func<DbSet<TEntity>, IQueryable<TResult>> query)
        {
            return await query.Invoke(_table).ToListAsync();
        }

        public async Task<TEntity> GetBy(Expression<Func<TEntity, bool>> predicate, Func<DbSet<TEntity>, IIncludableQueryable<TEntity, object>> query = null)
        {
            //Microsoft.EntityFrameworkCore.Query.EntityQueryModelVisitor+TransparentIdentifier exception occurs when CLR Exceptions is on.
            //Github issue link https://github.com/aspnet/EntityFrameworkCore/issues/14894 states this will be fixed in EFCore 3
            return await GetEntities(query).FirstOrDefaultAsync(predicate);
        }

        public async Task Update(IEntity updateRequest)
        {
            var entity = await _table.FindAsync(updateRequest.Id);
            await UpdateEntity(entity, updateRequest);
        }

        public async Task Update(IEntity updateRequest, Func<DbSet<TEntity>, IIncludableQueryable<TEntity, object>> query)
        {
            var entity = await query.Invoke(_table)
                .FirstOrDefaultAsync(s => (s as IEntity).Id == updateRequest.Id);
            await UpdateEntity(entity, updateRequest);
        }
      

        public async Task UpdateEntity(TEntity entity, object updateRequest)
        {

            _db.Attach(entity).State = EntityState.Modified;
            _mapper.Map(updateRequest, entity);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteEntity(int id)
        {
            var entity = await _table.FindAsync(id);
            if (entity == null) return;

            _db.Remove(entity);
            await _db.SaveChangesAsync();
        }

        private IQueryable<TEntity> GetEntities(
            Func<DbSet<TEntity>, IQueryable<TEntity>> query = null) => query?.Invoke(_table) ?? _table.AsQueryable();

    }
}
