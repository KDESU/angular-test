﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Routing;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductApi.Configuration;
using ProductApi.Repositories;
using ProductApi.Seeding;

namespace ProductApi.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddCorsWithPolicyOptions(this IServiceCollection services)
        {
            // Cors policy is added to controllers via [EnableCors("CorsPolicy")]
            // or .UseCors("CorsPolicy") globally
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            return services;
        }
        public static IServiceCollection AddDbContextWithNpgSql(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ProductContext>(options =>
                options
                    .SetCommonContextOptions()
                    .UseNpgsql(configuration.GetConnectionString("DefaultConnection")));

            return services;
        }

        public static IServiceCollection AddPerRequestInjections(this IServiceCollection services)
        {
            return services
                    .AddScoped<IProductCategoryProvider, ProductCategoryProvider>()
                    .AddScoped(typeof(GenericRepository<>))
                    .AddScoped<SeederService>()
               ;
        }

        public static IServiceCollection AddAutomapperWithConfiguration(this IServiceCollection services)
        {
            services.AddAutoMapper(new[]
            {
                typeof(AutomapperConfiguration)
              
            });

            return services;
        }



    }
}
