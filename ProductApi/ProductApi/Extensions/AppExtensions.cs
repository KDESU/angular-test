﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace ProductApi.Extensions
{
    public static class AppExtensions
    {
        public static void UseCorsWithGlobalPolicy(this IApplicationBuilder app)
        {
            app.UseCors("CorsPolicy");
        }

    }
}
