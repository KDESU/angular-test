﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace ProductApi.Models
{
    public class Product : IEntity
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }

        public double Price { get; set; }
        public int? CategoryId { get; set; }
        public int QuantityInStock { get; set; }

        public Category Category { get; set; }

    }
}
