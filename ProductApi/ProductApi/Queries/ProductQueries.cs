﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using ProductApi.Models;

namespace ProductApi.Queries
{
    public class ProductQueries
    {
        public static IIncludableQueryable<Product, object> WithCategory(DbSet<Product> table)
        {
            return table
                .Include(j => j.Category)
               ;
        }
    }
}
