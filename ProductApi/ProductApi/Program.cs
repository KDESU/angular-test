﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProductApi.Seeding;

namespace ProductApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            ProcessDbCommands(args, host);
            host.Run();
           
        }

        static void ProcessDbCommands(string[] args, IWebHost host)
        {
            if (!args.Contains("recreatedb")) return;


            var services = (IServiceScopeFactory)host.Services.GetService(typeof(IServiceScopeFactory));
            using (var scope = services.CreateScope())
            {
                Console.WriteLine("Recreating db");
                var db = scope.ServiceProvider.GetRequiredService<ProductContext>();
                db.Database.EnsureDeleted();
                db.Database.Migrate();
                var seederService = scope.ServiceProvider.GetRequiredService<SeederService>();
                seederService.Seed();
                Environment.Exit(0);
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
