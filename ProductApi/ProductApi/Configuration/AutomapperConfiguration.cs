﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProductApi.DTO;
using ProductApi.Models;

namespace ProductApi.Configuration
{
    public class AutomapperConfiguration : Profile
    {
        public AutomapperConfiguration()
        {
            CreateMap<Product, ProductDto>().ForMember(p => p.Category, opt => opt.MapFrom(p=> p.Category.Name));
        }
    }
}
